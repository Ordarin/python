

import com.sun.glass.ui.Pixels;
import java.io.*;
import java.nio.file.Files;



public class Main {
    
   static boolean next = false;
   static String st1="";
   static String st2="";
   
   static String glob = "";
   static String []temp;
   static Integer []mass;
   static int N = 0;
  static Integer []chetm;
  static Integer []nchetm;

  static void al()
  {
      int k=0;
      int l=0;
      int e = 0;
    String a="";
    String b="";
    int tmp = 0;
    int chet = 0;
    int ne_chet = 0;
    String stmp = "";
    
     N = new Integer(st1);
     temp = st2.split(" ");
     mass = new Integer[temp.length];
     
      
      for(int i = 0 ; i < temp.length;i++)
      {
          tmp = new Integer(temp[i]);
          if(tmp>=1&&tmp<=31)
          {
              if(tmp%2==0){
                 mass[i] = new Integer(temp[i]);
                 chet++;   
                 
              }
              if(tmp%2!=0){
                 mass[i] = new Integer(temp[i]);
                 ne_chet++;   
              }
          }
      }
      
      chetm = new Integer[chet];
      nchetm = new Integer[ne_chet];
     
      while(k<mass.length)
      {
          if(mass[k]%2==0){
              chetm[l]=mass[k];
              l++;
          }else
          if(mass[k]%2!=0)
          {
              nchetm[e]=mass[k];
              e++;
          }
          k++;
      }
     
      if(chet>=ne_chet){
          stmp = "YES";
      }else{
          stmp = "NO";
      }
      for(int i = 0;i<chet;i++){
          if(i!=chet-1)
          {
            a+=chetm[i]+" ";
          }else{
              a+=chetm[i]+"\r\n";
          }
          
      }
      for(int i = 0;i<ne_chet;i++){
          if(i!=ne_chet-1)
          {
            b+=nchetm[i]+" ";
          }else{
              b+=nchetm[i]+"\r\n";
          }
      }
      
        glob+=b+a+stmp;
  }
    
    public static void main(String[] args) {
      String Path = System.getProperty("user.dir")+"\\input.txt";
      try(FileReader reader = new FileReader(Path))
        {
            
            int temp;
            
            
            while((temp=reader.read())!=-1)
            {
           
                if((char)temp=='\n'){next = true;}
                if(next==false)
                {if((char)temp=='\r'){}
                else if((char)temp=='\n'){}else{
                 st1=st1+(char)temp;}
                }
                if(next == true)
                { if((char)temp=='\r'){}
                else if((char)temp=='\n'){}else{
                   st2=st2+(char)temp;}
                }     
           }     
        }
        catch
       (Exception e){}
        finally {}
      
        al();
        
       try(FileWriter writer = new FileWriter(System.getProperty("user.dir")+"\\output.txt", false))
        {
           
            String text = glob;
            writer.write(text);
            
        }
        catch(IOException ex){
             
            System.out.println(ex.getMessage());
        } 
    }
    }
    

